import React from 'react'
import { Container, Row, Col } from 'react-bootstrap'
import styled from 'styled-components'
import TitlePage from '../../../components/titlePage'


export default () => {
    return (
        <About>
            <TitlePage title="Sobre" sub="Conheça nossa história" />

            <Collaborators>
                <Container>
                    <h3>Nossos Colaboradores</h3>
                    <Row>
                        <BoxItem>João</BoxItem>
                        <BoxItem>Maria</BoxItem>
                        <BoxItem>José</BoxItem>
                        <BoxItem>Carlos</BoxItem>
                    </Row>
                </Container>

            </Collaborators>
        </About>
    )
}


const About = styled.div`
    display:block;
`
const Description = styled.div`
    height: 200px;
    background: #fff4;
    width: 100%;
    display:flex;
`
const Collaborators = styled.div`

    min-height: 200px;
    background: yellow;
    width: 100%;
    padding: 20px 0;
`
const BoxItem = styled(Col)`
    background: red;
    height: 200px;
    margin: 10px;
    width: 20%;
`

