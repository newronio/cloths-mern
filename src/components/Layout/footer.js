import React from 'react'
import { Nav } from 'react-bootstrap'
import styled from 'styled-components'
import { FaFacebook, FaInstagram, FaLinkedin } from "react-icons/fa";

const footer = () => {
    return (
        <Footer>
            <section className="company" id="company-info">
                    <div className="container">
                        <div className="row">
                            <div className="col-xs-12 col-sm-6 col-md-6 col-lg-6 center-xs center-sm center-md center-lg">
                                <h4>Contato</h4>
                                <ul>
                                    <li><i className="fa fa-phone"></i> +55(21)0000-0000</li>
                                    <li><i className="fa fa-envelope"></i> santannascompany@company.com</li>
                                    <li><i className="fa fa-map"></i> Brasil, Rio de Janeiro</li>
                                </ul>
                            </div>
                            <div className="col-xs-12 col-sm-6 col-md-6 col-lg-6 center-xs center-sm center-md center-lg">
                                <h4>Redes Sociais</h4>
                                <Nav defaultActiveKey="/home" className="flex-column">
                                <Nav.Link href="/"><FaFacebook/> Facebook</Nav.Link>
                                    <Nav.Link href="/"><FaInstagram/> Instagram</Nav.Link>
                                    <Nav.Link href="/"><FaLinkedin/> Linkedin</Nav.Link>
                                </Nav>
                            </div>
                        </div>
                    </div>

                    <div className="container">
                        <div className="row center-xs center-sm center-md center-lg">
                            <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12"></div>
                            <p>Copyright &copy; Daniel Santanna 2020</p>
                        </div>
                    </div>
                </section>
        </Footer>
    )
}

const Footer = styled.div `
    padding-top: 20px;
    font-size:15px;
    background-color:black;
    color: white;

    a {
        font-size: 1rem;
        text-transform: uppercase;
        font-weight: bold;
        letter-spacing: 0.5rem;
        transition: color 0.3s linear;
      }

    .container {
        font-weight: bold;
        letter-spacing: 0.1rem;
        transition: color 0.3s linear;
      }
`




export default footer
