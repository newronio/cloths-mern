import React from 'react'
import { Nav, NavDropdown } from 'react-bootstrap'
import { getUser, removeToken } from '../../../config/auth'

const Header = () => {

    return (
        <Nav className="mr-auto navbar navbar-expand bg-white mb-4">
            <NavDropdown title={getUser().name} id="dropdown-basic" className="ml-auto">
                <NavDropdown.Item href="#action/3.3">Perfil</NavDropdown.Item>
                <NavDropdown.Divider />
                <NavDropdown.Item href="/" onClick={removeToken}>Sair</NavDropdown.Item>
            </NavDropdown>
        </Nav>
    )
}
export default Header
