import React from 'react'
import '../../../assets/styles/aaa.css'

import Sidebar from './sidebar'
import Header from './header'
const LayoutAdmin = ({ children }) => {
    return (
        <div id="wrapper">
            <Sidebar />
            <div id="content-wrapper" className="d-flex flex-column">
                <div id="content">
                    <Header />
                    <div className="container-fluid">
                        {children}
                    </div>
                </div>

            </div>
        </div>

    )
}

export default LayoutAdmin
